#!/bin/bash

DOCKERFILE="Dockerfile.default"
IMAGE_VERSION="latest"
EXPORT_VERSION=""
MODE="develop"
ENV_INSTALL=""
ENV_VERSION=""
ENV_LOAD=""

IMAGE_VERSION_ARG="--build-arg image_version=latest"
EXPORT_VERSION_ARG=""
MODE_ARG="--build-arg mode=develop"
ENV_INSTALL_ARG=""
ENV_VERSION_ARG=""
ENV_LOAD_ARG=""

PP=( "$@" )

if [[ " ${PP[*]} "  =~ " --help " ]]; then
	echo "Supported Arguments: "
    echo "  --docker_file=[name]                The docker file to build                (default=Dockerfile.default)"
    echo "  --image_version=[name]              Package versions to build               (default=latest)"
    echo "  --export_version=[name]             Name to assign to exported version      (default=None)"
    echo "  --mode=[mode]                       Wether to mount in build/install        (default=develop)"
    echo "  --env_install=[env_name]		Environment to install                  (default=None)"
    echo "  --env_load=[env_name]		Environment to load                     (default=None)"
    echo "  --env_version=[env_version]		Environment version                     (default=None)"
fi


if [[ " ${PP[*]} " =~ --docker_file=([^[:space:]]+) ]]; then
	DOCKERFILE="${BASH_REMATCH[1]}"
fi

if [[ " ${PP[*]} " =~ --image_version=([^[:space:]]+) ]]; then
	IMAGE_VERSION="${BASH_REMATCH[1]}"
    IMAGE_VERSION_ARG="--build-arg image_version=${IMAGE_VERSION}"
fi

if [[ " ${PP[*]} " =~ --export_version=([^[:space:]]+) ]]; then
    EXPORT_VERSION="${BASH_REMATCH[1]}"
	EXPORT_VERSION_ARG="--build-arg export_version=${EXPORT_VERSION}"
fi

if [[ " ${PP[*]} " =~ --mode=([^[:space:]]+) ]]; then
	MODE="${BASH_REMATCH[1]}"
    MODE_ARG="--build-arg mode=${BASH_REMATCH[1]}"
fi

if [[ " ${PP[*]} " =~ --env_install=([^[:space:]]+) ]]; then
    	ENV_INSTALL="${BASH_REMATCH[1]}"
	ENV_INSTALL_ARG="--build-arg env_install=${ENV_INSTALL}"
fi

if [[ " ${PP[*]} " =~ --env_version=([^[:space:]]+) ]]; then
    	ENV_VERSION="${BASH_REMATCH[1]}"
	ENV_VERSION_ARG="--build-arg env_version=${ENV_VERSION}"
fi

if [[ " ${PP[*]} " =~ --env_load=([^[:space:]]+) ]]; then
    ENV_LOAD="${BASH_REMATCH[1]}"
	ENV_LOAD_ARG="--build-arg env_load=${ENV_LOAD}"
fi

DYN_PROCS_COMMIT=$(echo $(git submodule status) | grep -o '^[0-9a-f]*')
DYN_PROCS_COMMIT_ARG="--build-arg dyn_procs_commit=$DYN_PROCS_COMMIT"

ARGS="$MODE_ARG $IMAGE_VERSION_ARG $EXPORT_VERSION_ARG $ENV_INSTALL_ARG $ENV_VERSION_ARG $ENV_LOAD_ARG $DYN_PROCS_COMMIT_ARG"
echo "Bulding image docker-cluster:$(echo $DOCKERFILE | grep -o "\..*$" | sed 's/^\.//')-$IMAGE_VERSION-e$EXPORT_VERSION-i$ENV_INSTALL-v$ENV_VERSION-l$ENV_LOAD-$MODE $DYN_PROCS_COMMIT_ARG from $DOCKERFILE ... with args $ARGS"
#--no-cache-filter=export
DOCKER_BUILDKIT=1 docker build $ARGS --no-cache-filter=export  -t "docker-cluster:$(echo $DOCKERFILE | grep -o "\..*$" | sed 's/^\.//')-$IMAGE_VERSION-e$EXPORT_VERSION-i$ENV_INSTALL-v$ENV_VERSION-l$ENV_LOAD-$MODE" -f $DOCKERFILE . > tmp/build_output.log 2>&1

if [ "$EXPORT_VERSION" != "" ]; then
    test_var=$(<tmp/build_output.log)
    echo "Captured"
    echo $test_var

    pat1="===DPP-DNF-$EXPORT_VERSION==="
    pat2="===DPP-PIP-$EXPORT_VERSION==="
    echo "# DNF PACKAGES" > export/version-$EXPORT_VERSION.txt
    dnf_packages=$(echo $test_var   | sed "s/$pat1/\n/3; s/$pat1/\n/4; s/[^\n]*\n//; s/\n.*//" \
                            | sed "s/$pat1.*//" | grep -oP '\b[a-zA-Z0-9_.+-]+(?:\+\+)?-\d+[\w.-]*') >> export/version-$EXPORT_VERSION.txt
    for package in $dnf_packages; do
    	echo "$package" >> export/version-$EXPORT_VERSION.txt
    done
    echo "# PIP PACKAGES" >> export/version-$EXPORT_VERSION.txt
    pip_packages=$(echo $test_var   | sed "s/$pat2/\n/2; s/$pat2/\n/3; s/[^\n]*\n//; s/\n.*//" \
                            | sed 's/.*===DPP-PIP-1.0.0===//')
    for package in $pip_packages; do
    	echo "$package" >> export/version-$EXPORT_VERSION.txt
    done
 

fi


