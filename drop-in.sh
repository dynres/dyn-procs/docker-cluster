#!/bin/bash

USER=mpiuser
HOST=n1
DROP_IN_DIR=/opt/hpc/build/dyn_procs_setup

PP=( "$@" )
if [[ " ${PP[*]} "  =~ " --help " ]]; then
	echo "Supported Arguments: "
    echo "  --user=[username]              Drop in as user [user]                  (default=mpiuser)"
    echo "  --host=[hostname]              Drop into host [hostname]               (default=n1)"
fi
if [[ " ${PP[*]} " =~ --user=([^[:space:]]+) ]]; then
	USER="${BASH_REMATCH[1]}"
fi
if [[ " ${PP[*]} " =~ --host=([^[:space:]]+) ]]; then
	HOST="${BASH_REMATCH[1]}"
fi

HOST_INDEX=$(echo "$HOST" | grep -oE '[0-9]+$')
C_NAME=$(cat tmp/shutdown-cluster.sh | head -n$HOST_INDEX | tail -n1 | grep -oE '\w+$')

# Delete last exported env if we are in developer mode
if [[ $(docker ps --filter "id=$C_NAME" --format "{{.Image}}") =~ develop$ ]]; then
	if [ -f ./build/dyn_procs_setup/export/exported_envs/load_exported_env.sh ]; then
		rm -rf ./build/dyn_procs_setup/export/exported_envs/load_exported_env.sh
	fi
fi

# Fallback to a save drop in dir
if [ ! -d ./build/dyn_procs_setup ]; then
	DROP_IN_DIR=/opt/hpc
fi
    
echo "Dropping into host '$HOST' as user '$USER'"
docker exec -it -u $USER -w $DROP_IN_DIR --env COLUMNS=`tput cols` --env LINES=`tput lines` $C_NAME bash
