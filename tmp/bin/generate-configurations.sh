#!/usr/bin/env bash

# adjust these based on how much you want to share with each container node
CORES_PER_NODE=8
MB_PER_NODE=2048

# processing host-file
echo "processing the provided hostfile "
echo "getting unique entries..."
awk '!a[$0]++' ./tmp/hostfile.txt > ./tmp/unique_hosts
awk 'NR>1' ./tmp/unique_hosts > ./tmp/compute_hosts

echo "setting up NodeName and PartitionName entries in slurm.conf ..."
rm -f ./tmp/hosts
rm -f ./tmp/nodes
rm -f ./tmp/all_nodes
rm -f ./tmp/hostuser
rm -f ./tmp/prrte-hostfile.txt

HostNumber=1
FormattedNumber=""
while read h; do
	FormattedNumber=`printf %01d $HostNumber`
	echo "$h	n${FormattedNumber}" >> ./tmp/hosts
	echo "n${FormattedNumber}" >> ./tmp/all_nodes
	((HostNumber++))
done < ./tmp/unique_hosts

HostNumber=2
FormattedNumber=""
while read h; do
	FormattedNumber=`printf %01d $HostNumber`
	echo "n${FormattedNumber}" >> ./tmp/nodes
	echo "n${FormattedNumber} slots=${CORES_PER_NODE}" >> ./tmp/prrte-hostfile.txt
	((HostNumber++))
done < ./tmp/compute_hosts
echo "${USER}" >> ./tmp/hostuser



mkdir -p ./tmp/mount/etc/
cp -a ./tmp/unique_hosts ./tmp/mount/etc/unique_hosts
cp -a ./tmp/hosts ./tmp/mount/etc/hosts
cp -a ./tmp/nodes ./tmp/mount/etc/nodes
cp -a ./tmp/nodes ./tmp/mount/etc/mrnet-hostfile.txt
cp -a ./tmp/prrte-hostfile.txt ./tmp/mount/etc/prrte-hostfile.txt
cp -a ./tmp/hostuser ./tmp/mount/etc/hostuser

printf "\nhosts:\n"
cat ./tmp/mount/etc/hosts
printf "\nhost user:\n"
cat ./tmp/mount/etc/hostuser

exit 0
