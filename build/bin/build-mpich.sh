#!/bin/bash -xe

./autogen.sh

source_dir=$PWD

echo "building MPICH from ${source_dir}"
rm -rf ../mpich-build
mkdir  ../mpich-build
cd     ../mpich-build

${source_dir}/configure --prefix=${MPICH_ROOT} --with-pm=no --with-pmi=pmix --with-pmix=${PMIX_ROOT} --with-slurm=${SLURM_ROOT} --with-device=ch4:ofi \
            2>&1 | tee configure.log.$$ 2>&1
make -j $1 2>&1 | tee make.log.$$ 2>&1
make -j install 2>&1 | tee make.install.log.$$
