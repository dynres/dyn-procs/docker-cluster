#!/bin/bash -xe

./configure --prefix=${MRNET_ROOT} \
            2>&1 | tee configure.log.$$ 2>&1
make -j 2>&1 | tee make.log.$$ 2>&1
make -j install 2>&1 | tee make.install.log.$$
make -j install-examples 2>&1 | tee make.install.examples.log.$$
make -j install-tests 2>&1 | tee make.install.tests.log.$$
