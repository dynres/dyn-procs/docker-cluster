#!/bin/bash -xe

./autogen.sh
./configure --prefix=${GPI2_ROOT} \
            --with-ethernet \
            --with-slurm \
            2>&1 | tee configure.log.$$ 2>&1
make -j 2>&1 | tee make.log.$$ 2>&1
make -j install 2>&1 | tee make.install.log.$$
