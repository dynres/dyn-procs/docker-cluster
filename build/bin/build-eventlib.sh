#!/bin/bash -xe

source_dir=$PWD

echo "building libevent from ${source_dir}"
rm -rf ../eventlib-build
mkdir  ../eventlib-build
cd     ../eventlib-build

cmake -DCMAKE_INSTALL_PREFIX:PATH=${LIBEVENT_ROOT} -S${source_dir} -B. \
            2>&1 | tee configure.log.$$ 2>&1
make -j 2>&1 | tee make.log.$$ 2>&1
make -j install 2>&1 | tee make.install.log.$$
