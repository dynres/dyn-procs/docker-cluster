#!/bin/bash -xe

./autogen.pl

source_dir=$PWD

echo "building Open PMIx from ${source_dir}"
rm -rf ../openpmix-build
mkdir  ../openpmix-build
cd     ../openpmix-build

#${source_dir}/configure --prefix=${PMIX_ROOT} --enable-debug --disable-debug-symbols --with-libevent=${LIBEVENT_ROOT} \
${source_dir}/configure --prefix=${PMIX_ROOT} --enable-debug --disable-debug-symbols \
            2>&1 | tee configure.log.$$ 2>&1
make -j 2>&1 | tee make.log.$$ 2>&1
make -j install 2>&1 | tee make.install.log.$$
