#!/bin/bash -xe

# The configuration of the plugins is similar to that of Slurm,
# but we include an autogen script, and the
# location of Slurm's source tree must be specified.
./autogen.sh

source_dir=$PWD

echo "building DEEP-SEA's Slurm plugins from ${source_dir}"
rm -rf ../plugins-build
mkdir  ../plugins-build
cd     ../plugins-build

# need to install in the same location as slurm with --prefix
${source_dir}/configure --prefix=${SLURM_ROOT} \
            --with-slurmsrc=/opt/hpc/build/slurm/ \
            --with-slurmbuild=/opt/hpc/build/slurm-build/ \
			--with-libevent=${LIBEVENT_ROOT} \
            --with-libdynpm=${LIBDYNPM_ROOT} \
            --with-pmix=${PMIX_ROOT} \
            --enable-silent-rules \
            2>&1 | tee configure.log.$$ 2>&1
make -j 2>&1 | tee make.log.$$ 2>&1
make -j install 2>&1 | tee make.install.log.$$
