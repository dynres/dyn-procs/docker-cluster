#!/bin/bash -xe

./autogen.pl

source_dir=$PWD

echo "building Open MPI from ${source_dir}"
rm -rf ../ompi-build
mkdir  ../ompi-build
cd     ../ompi-build

${source_dir}/configure --prefix=${OMPI_ROOT} \
            --with-libevent=${LIBEVENT_ROOT} \
            --with-pmix=${PMIX_ROOT} \
            --with-prrte=${PRRTE_ROOT}\
	    --with-slurm=yes \
            --enable-mpirun-prefix-by-default \
            2>&1 | tee configure.log.$$ 2>&1
make -j 2>&1 | tee make.log.$$ 2>&1
make -j install 2>&1 | tee make.install.log.$$
