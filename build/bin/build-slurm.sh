#!/bin/bash -xe

# Slurm code is distributed with pre-generated autotools based buildsystem

# For DEEP-SEA, we have stripped the pre-generated files; therefore, they
# need to be generated before calling configure
# update 22.4.2022: upstream has accepted the pmix v5 patch (sorted until Open PMIx v6)
# autoreconf -fi

source_dir=$PWD

echo "building Slurm from ${source_dir}"
rm -rf ../slurm-build
mkdir  ../slurm-build
cd     ../slurm-build

${source_dir}/configure --prefix=${SLURM_ROOT} --with-pmix=${PMIX_ROOT} --enable-silent-rules \
            2>&1 | tee configure.log.$$ 2>&1
make -j 2>&1 | tee make.log.$$ 2>&1
make -j install 2>&1 | tee make.install.log.$$
