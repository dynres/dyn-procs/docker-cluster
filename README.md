# DPP Docker Cluster

## Derived Work

This development and testing environment is derived work from:

https://github.com/jjhursey/pmix-swarm-toy-box

Especial thanks to Josh Hursey and Isaias Compres for kindly allowing us to reuse this work.

## Quick Steps for the dyn_procs Setup
These quick steps are required to setup the dyn_procs environment.
More details can be found in the next sections.

It is recommended to use your own regular user, and not root. 
Add your user to the docker group:
```
usermod -aG docker $USER
```

Initialize the swarm cluster:

```
docker swarm init
```

Build the docker image:
```
./docker-build.sh
```
(By the default the build and install directories will be mounted at /opt/hpc inside of the docker-cluster)

Start the cluster [-n: number of nodes (recommended: 8)]: 
```
./start.sh -n 8
```

Drop into the cluster:
```
./drop-in.sh
```

**Install and load packages as described in the dyn_procs_setup [README](https://gitlab.inria.fr/dynres/dyn-procs/dyn_procs_setup/-/tree/docker_setup?ref_type=heads).**


## More Details about the Docker Environment

## Installing Docker

Follow the Docker installation instructions for your Linux distribution.  For example, Ubuntu:
https://docs.docker.com/engine/install/ubuntu/

## Setup your development environment outside the container

We try to keep the container image as small as possible.
For this, the source code and build files are kept in the host system, and mounted in the container.
We will use volume mounts for this purpose. 
We are using the local disk as a shared file system between the host and the containers.

The key to making this work is that you can edit the source code outside of the container, but all builds must occur inside the container. 
This is because the relative paths to dependent libraries and install directories are relative to the paths inside the container's file system not the host file system.

Note that this will work when using Docker Swarm on a single machine. More work is needed if you are running across multiple physical machines.

### Checkout your software in the 'build/' directory

For ease of use, we will checkout the software into a `$TOPDIR/build` subdirectory. 
`$TOPDIR` is where this `README.md` file is located. 
We will mount this directory in `/opt/hpc/build` inside the container. 

### A common 'install/' directory is defined in the build scripts

This directory serves as the shared install file system for the builds. 
We mount this directory in `/opt/hpc/install` inside the container. 
The container's environment is setup taking these paths into account.
